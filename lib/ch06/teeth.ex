defmodule Teeth do

  @spec pocket_depths() :: [[number()]]
  def pocket_depths do
    [[0], [2,2,1,2,2,1], [3,1,2,3,2,3],
    [3,1,3,2,1,2], [3,2,3,2,2,1], [2,3,1,2,1,1],
    [3,1,3,2,3,2], [3,3,2,1,3,1], [4,3,3,2,3,3],
    [3,1,1,3,2,2], [4,3,4,3,2,3], [2,3,1,3,2,2],
    [1,2,1,1,3,2], [1,2,2,3,2,3], [1,3,2,1,3,3], [0],
    [3,2,3,1,1,2], [2,2,1,1,3,2], [2,1,1,1,1,2],
    [3,3,2,1,1,3], [3,1,3,2,3,2], [3,3,1,2,3,3],
    [1,2,2,3,3,3], [2,2,3,2,3,3], [2,2,2,4,3,4],
    [3,4,3,3,3,4], [1,1,2,3,1,2], [2,2,3,2,1,3],
    [3,4,2,4,4,3], [3,3,2,1,2,3], [2,2,2,2,3,3],
    [3,2,3,2,3,2]]
  end


  @spec alert([[number()]]) :: [number()]
  def alert(list) do
    alert(list, 1, [])
  end

  @spec alert([[number()]], number(), [number()]) :: [number()]
  def alert([h|t], num, res) do
    if Stats.maximum(h) >= 4 do
      alert(t, num + 1, [num | res])
    else
      alert(t, num + 1, res)
    end
  end

  def alert([], _, res) do
    Enum.reverse(res)
  end

end
