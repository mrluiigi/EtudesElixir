defmodule NonFP do

  # Exemplo
  # NonFP.generate_pockets('FTTTTTTTTTTTTTTFTTTTTTTTTTTTTTTT', 0.5)

  @spec generate_pockets([char()], number()) :: [[number()]]
  def generate_pockets(teeth, prob_good) do
    generate_pockets(teeth, prob_good, [])
  end


  @spec generate_pockets([char()], number(), [[number()]]) :: [[number()]]
  def generate_pockets([], _, acc) do
    Enum.reverse(acc)
  end

  def generate_pockets([head|tail], prob, acc) when head == ?T do
    tooth = generate_tooth(prob)
    generate_pockets(tail, prob, [tooth | acc])

  end

  def generate_pockets([head|tail], prob, acc) when head == ?F do
    generate_pockets(tail, prob, [[0] | acc])
  end


  @spec generate_tooth(number()) :: [number()]
  def generate_tooth(prob) do
    random = :random.uniform()
    if (random < prob) do
      base_depth = 2
      generate_tooth(base_depth, 6, [])
    else
      base_depth = 3
      generate_tooth(base_depth, 6, [])
    end
  end

  @spec generate_tooth(number(), number(), [number()]) :: [number()]
  def generate_tooth(base, item, acc) do
    if item == 0 do
      acc
    else
      random = :random.uniform(3) - 2
      generate_tooth(base, item - 1, [base + random | acc])
    end
  end


end
