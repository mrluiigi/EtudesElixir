defmodule Dates6 do

  @spec julian(String.t()) :: number()
  def julian(data) do
    [y, m, d] = date_parts(data)
    days_per_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    total = somaDias(m, y, days_per_month, 0)
    res = total + d
    res
  end


  @spec date_parts(String.t()) :: [number()]
  defp date_parts(data) do
    [y, m, d] = String.split(data, "-")
    [String.to_integer(y), String.to_integer(m), String.to_integer(d)]
  end


  @spec somaDias(number(), number(), [number()], number()) :: number()
  defp somaDias(m, y, arr, acc) do

    if m == 1 do
      acc
    else

      [h|t] = arr
      if m == 2 and is_leap_year(y) do
        somaDias(m-1, y, t, h+acc+1)
      else
        somaDias(m-1, y, t, h+acc)
      end
    end

  end

  @spec is_leap_year(number()) :: boolean()
  defp is_leap_year(year) do
    rem(year,4) == 0 and (rem(year, 100) != 0) or rem(year, 400) == 0
  end

end
