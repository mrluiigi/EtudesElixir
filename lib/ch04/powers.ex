defmodule Power do
  import Kernel, except: [raise: 2, raise: 3]


  @spec raise(number(), number()) :: number()
  def raise(x, n) do
      cond do
        n == 0  ->  1
        n == 1  ->  x
        n > 0   ->  raise(x, n, 1)
        n < 0   ->  1.0/raise(x, -n)
      end
  end


  @spec raise(number(), number(), number()) :: number()
  def raise(x, n, acc) do
    cond do
      n == 0  ->  acc
      n != 0  ->  raise(x, n-1, x*acc)
    end
  end



  @spec nth_root(number(), number()) :: number()
  def nth_root(x, n) do
    nth_root(x, n, x / 2.0)
  end


  @spec nth_root(number(), number(), number()) :: number()
  def nth_root(x, n, a) do
    IO.puts("Current guess is #{a}")
    f = raise(a, n) - x
    f_prime = n * raise(a, n-1)
    next = a-f/f_prime
    change = abs(next-a)
    if change < 1.0e-8 do
      next
    else
      nth_root(x, n, next)
    end

  end

end
