defmodule Geom2 do

  @spec area(number(), number()) :: number()
  def area(length \\ 1, width \\ 1) do    # length \\ 1 sets 1 as length default value, same for width
    length * width
  end
end
