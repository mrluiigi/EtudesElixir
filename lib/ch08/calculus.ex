defmodule Calculus do

  @spec derivative(fun(), number()) :: number()
  def derivative(function, point) do
    delta = 1.0e-10
    (function.(point+delta)-function.(point))/delta
  end

end
