defmodule Cards do

  @doc """
  Create a deck of 52 tuples
  """
  @spec make_deck() :: [{String.t(), String.t()}]
  def make_deck() do
    for x <- [2,3,4,5,6,7,8,9,10,"J","Q","K","A"],
        y <- ["Clubs", "Diamonds", "Hearts", "Spades"], do:
        [{x, y}]
  end


  @spec shuffle([{String.t(), String.t()}]) :: [{String.t(), String.t()}]
  def shuffle(list) do
    :random.seed(:erlang.now())
    shuffle(list, [])
  end

  @spec shuffle([{String.t(), String.t()}], [{String.t(), String.t()}]) :: [{String.t(), String.t()}]
  def shuffle([], acc) do
    acc
  end

  def shuffle(list, acc) do
    {leading, [h | t]} =
      Enum.split(list, :random.uniform(Enum.count(list)) - 1)
      shuffle(leading ++ t, [h | acc])
  end
end
