defmodule Dates8 do

  @spec julian(String.t()) :: number()
  def julian(data) do
    [y, m, d] = date_parts(data)
    days_per_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    {f, _s} = Enum.split(days_per_month, m-1)
    accTotal = List.foldl(f, 0, fn(x, acc) -> x+acc end)

    if is_leap_year(y) and m > 2 do
      accTotal+d+1
    else
      accTotal+d
    end
  end


  @spec date_parts(String.t()) :: [number()]
  defp date_parts(data) do
    [y, m, d] = String.split(data, "-")
    [String.to_integer(y), String.to_integer(m), String.to_integer(d)]
  end

  @spec is_leap_year(number()) :: boolean()
  defp is_leap_year(year) do
    rem(year,4) == 0 and (rem(year, 100) != 0) or rem(year, 400) == 0
  end

end
