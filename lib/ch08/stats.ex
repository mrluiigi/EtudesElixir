defmodule Stats8 do

   @spec minimum([number()]) :: number()
  def minimum(list) do
    [h|t] = list
    minimum( t, h )
  end

  @spec minimum([number()], number()) :: number()
  def minimum([], res) do
    res
  end

  def minimum([h|t], res) do
    if h < res do
      minimum(t, h)
    else
      minimum(t, res)
    end
  end


  @spec maximum([number()]) :: number()
  def maximum(list) do
    [h|t] = list
    maximum(t, h)
  end

  @spec maximum([number()], number()) :: number()
  def maximum([], res) do
    res
  end

  def maximum([h|t], res) do
    if h > res do
      maximum(t, h)
    else
      maximum(t, res)
    end
  end


  @spec range([number()]) :: [number()]
  def range(list) do
    [minimum(list), maximum(list)]
  end


  @spec mean([number()]) :: number()
  def mean(list) do
    List.foldl(list, 0, fn(x, acc) -> x + acc end) / Enum.count(list)
  end

  @spec stdv([number()]) :: number()
  def stdv(list) do
    n = Enum.count(list)
    sum = List.foldl(list, 0, fn(x, acc) -> x + acc end)
    sum_sq = List.foldl(list, 0, fn(x, acc) -> x*x + acc end)
    :math.sqrt((n * sum_sq - sum * sum)/(n*(n-1)))
  end

end
