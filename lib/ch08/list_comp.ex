defmodule ListComp do

  @spec people() :: [{String.t(), String.t(), number()}]
  defp people() do
    [{"Federico", "M", 22}, {"Kim", "F", 45}, {"Hansa", "F", 30},
      {"Tran", "M", 47}, {"Cathy", "F", 32}, {"Elias", "M", 50}]
  end


  @spec over40() :: [{String.t(), String.t(), number()}]
  def over40() do
    for {name, gender, age} <- people(), age > 40, gender == "M"
                            do {name, gender, age}
    end
  end


  @spec male_or_over40() :: [String.t()]
  def male_or_over40() do

    for {name, gender, age} <- people(), age > 40 or gender == "M"
                            do {name}
    end

  end

end
