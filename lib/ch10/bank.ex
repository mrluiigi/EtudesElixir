defmodule Bank do

  @spec account(number()) :: nil
  def account(balance) do
    input = IO.gets("D)eposit, W)ithdraw, B)alance, Q)uit: ")
    action = String.trim(String.upcase(input))
    if(action != "Q") do
      new_balance = transaction(balance, action)
      account(new_balance)
    end
  end

  @spec transaction(number(), String.t()) :: number()
  def transaction(balance, action) do
    case action do
      "D" ->
              input = IO.gets("Amount to deposit: ")
              deposit = get_number(input)
              cond do
                deposit >= 10000 ->
                                  :error_logger.warning_msg("Larger amount to deposit $#{deposit}")
                                  IO.puts("Your deposit of $#{deposit} may be subject to hold.")
                                  new_balance = balance + deposit
                                  IO.puts("Your new balance is $#{new_balance}")
                                  new_balance
                deposit < 0      ->
                                  :error_logger.error_msg("Deposits may not be less than zero.")
                                  IO.puts("Deposits may not be less than zero.")
                                  balance
                deposit >= 0     ->
                                  :error_logger.info_msg("Successful deposit of $#{deposit}")
                                  new_balance = balance + deposit
                                  IO.puts("Your new balance is #{new_balance}")
                                  new_balance
              end
      "W" ->
            input = IO.gets("Amount to withdraw: ")
            withdraw = get_number(input)
            cond do
              withdraw < 0       ->
                                    :error_logger.warning_msg("Negative withdrawal amount $#{withdraw}")
                                    IO.puts("Withdrawals may not be less than zero")
                                    balance
              withdraw > balance ->
                                    :error_logger.error_msg("Overdraw $#{withdraw} from $#{balance}")
                                    IO.puts("You cannot withdraw more than your current balance of $#{balance}")
                                    balance
              withdraw > 0 and withdraw < balance ->
                                                    :error_logger.info_msg("Successful withdrawal $#{withdraw}")
                                                    new_balance = balance - withdraw
                                                    IO.puts("Your new balance is $#{new_balance}")
                                                    new_balance
            end
      "B" ->
              IO.puts("Your current balance is $#{balance}")
              balance
    end
  end


  @spec get_number(String.t()) :: number()
  def get_number(value) do
    a = String.trim(value)
    cond do
      Regex.match?(~r/^[+-]?\d+$/, a)                      ->  String.to_integer(a)               # int
      Regex.match?(~r/^[+-]?\d+\.\d+([eE][+-]?\d+)?$/, a)  -> String.to_float(a)                  # float
      true -> :error
    end
  end

end
