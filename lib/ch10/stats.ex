defmodule Stats10 do

  @spec minimum([number()]) :: number()
  def minimum(list) do
    try do
      [h|t] = list
      minimum( t, h )
    rescue
      err -> err
    end
  end

  @spec minimum([number()], number()) :: number()
  def minimum([], res) do
    res
  end

  def minimum([h|t], res) do
    if h < res do
      minimum(t, h)
    else
      minimum(t, res)
    end
  end


  @spec maximum([number()]) :: number()
  def maximum(list) do
    try do
      [h|t] = list
      maximum(t, h)
    rescue
      err -> err
    end

  end

  @spec maximum([number()], number()) :: number()
  def maximum([], res) do
    res
  end

  def maximum([h|t], res) do
    if h > res do
      maximum(t, h)
    else
      maximum(t, res)
    end
  end


  @spec range([number()]) :: [number()]
  def range(list) do
    [minimum(list), maximum(list)]
  end



  @spec mean([number()]) :: number()
  def mean(list) do
    try do
      List.foldl(list, 0, fn(x, acc) -> x + acc end) / Enum.count(list)
    rescue
      err -> err
    end
  end

  @spec stdv([number()]) :: number()
  def stdv(list) do
    try do
      n = Enum.count(list)
      sum = List.foldl(list, 0, fn(x, acc) -> x + acc end)
      sum_sq = List.foldl(list, 0, fn(x, acc) -> x*x + acc end)
      :math.sqrt((n * sum_sq - sum * sum)/(n*(n-1)))
    rescue
      err -> err
    end

  end

end
