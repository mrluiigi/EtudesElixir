defmodule Cards9 do

  @doc """
  Create a deck of 52 tuples
  """
  @spec make_deck([any], [String.t()]) :: [{String.t(), String.t()}]
  def make_deck(values, suits) do
    for x <- values,
        y <- suits, do:
        [{x, y}]
  end


  @spec shuffle([{String.t(), String.t()}]) :: [{String.t(), String.t()}]
  def shuffle(list) do
    :random.seed(:erlang.now())
    shuffle(list, [])
  end

  @spec shuffle([{String.t(), String.t()}], [{String.t(), String.t()}]) :: [{String.t(), String.t()}]
  def shuffle([], acc) do
    acc
  end

  def shuffle(list, acc) do
    {leading, [h | t]} =
      Enum.split(list, :random.uniform(Enum.count(list)) - 1)
      shuffle(leading ++ t, [h | acc])
  end
end
