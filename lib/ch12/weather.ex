defmodule Weather do

  use GenServer

  def start_link() do   ## Start the server. Link the processes, if one dies, the other dies too
    GenServer.start_link(__MODULE__, [], [{:name, __MODULE__}])    ## second argument will be passed to the init function (Empty list)
  end



  def report(station) do
    GenServer.call(__MODULE__, station)
  end

  @doc "See list of most recently viewed stations"
  def recent() do
    GenServer.cast(__MODULE__, "")
  end


  def init([]) do     ## init the state of server. Initial state is an empty list
    :inets.start()
    {:ok, []}
  end

  def handle_call(request, _from, state) do
    {reply, new_state} = get_weather(request, state)
    {:reply, reply, new_state}      ## response is a reply, second is the reply and the third is the new state of the list
  end

  def handle_cast(_request, state) do
    IO.puts("Recently viewed: #{inspect(state)}")
    {:noreply, state}
  end


  defp get_content(element_name, xml) do
    {_, pattern} = Regex.compile("<#{element_name}>([^<]+)</#{element_name}>")
    result = Regex.run(pattern, xml)

    case result do
      [_all, match] -> {String.to_atom(element_name), match}
      nil -> {element_name, nil}
    end

  end


  def get_weather(station, state) do
    url = "http://w1.weather.gov/xml/current_obs/" <> station <> ".xml"

    url_header = [{'User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'}]

    {status, data} = :httpc.request(:get, {to_charlist(url), url_header}, [], [])

    case status do
      :error  ->  {{status, data}, state}
      :ok     ->
                  {{_http, code, _message}, _attrs, xml_as_chars} = data
                  case code do
                    200 ->  xml = to_string(xml_as_chars)
                            reply = {:ok, (for item <- [:location, :observation_time_rfc822, :weather, :temperature_string], do:
                                                      get_content(item, xml))}

                            new_state = [station | Enum.take(state, 9)]

                            {reply, new_state}

                    _   -> {{:error, code}, state}
                  end
    end
  end

end
