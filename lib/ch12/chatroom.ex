defmodule Chatroom do

  def start_link() do
    GenServer.start_link(__MODULE__, [], [{:name, __MODULE__}])
  end


  def init(state) do
    {:ok, state}
  end

  def handle_call({:login, user_name, server_name}, from, state) do
    {pid, _refnum} = from

    key = {user_name, server_name}
    IO.puts("#{user_name}@#{server_name} logging in.")
    if List.keymember?(state, key, 0) do
      reply = {:error, "#{user_name}@#{server_name} already logged in."}
      {:reply, reply, state}
    else
      reply = {:ok, "#{user_name}@#{server_name} logged in."}
      {:reply, reply, [{key, pid} | state]}
    end
  end

  def handle_call({:logout}, from, state) do
    {pid, _refnum} = from

    result = List.keyfind(state, pid, 1)
    case result do
      nil                     ->
                                reply = {:error, "Not logged in"}
                                {:reply, reply, state}
      {{user, server}, _pid}  ->
                                reply = {:ok, "#{user}@#{server} logged out"}
                                new_state = List.keydelete(state, pid, 1)
                                {:reply, reply, new_state}
    end
  end

  def handle_call({:say, text}, from, state) do
    {pid, _refnum} = from

    person = List.keyfind(state, pid, 1)

    case person do
      {{_user, _server}, _pid}  ->
                                Enum.map(state, fn x -> send_message(x, person, text) end)
                                reply = {:ok, "Message sent."}
                                {:reply, reply, state}
      nil                     ->
                                reply = {:error, "Unknown sender pid"}
                                {:reply, reply, state}
    end
  end

  def handle_call({:profile, person, server_name}, _from, state) do
    person = List.keyfind(state, {person, server_name}, 0)
    case person do
      nil                   ->
                              reply = {:error, "User doesn't exist"}
                              {:reply, reply, state}

      {{_user, _server}, pid} ->
                                reply = GenServer.call(pid, :get_profile)
                                {:reply, reply, state}            ##Verificar melhor o que devolver

    end
  end

  def send_message(receiver, sender, text) do
    {{_r_user, _r_server}, r_pid} = receiver
    {{s_user, s_server}, s_pid} = sender
    if r_pid != s_pid do
      GenServer.cast(s_pid, {:message, {s_user, s_server, text}})
    end
  end



end
