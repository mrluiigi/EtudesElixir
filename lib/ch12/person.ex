defmodule Person do

  def start_link(chat) do
    GenServer.start_link(__MODULE__, [chat], [{:name, __MODULE__}])
  end

  def init([chat]) do
    {:ok, %{server: {Chatroom, chat}, profile: Map.new()}}
  end


  def handle_call({:get_chat_node}, _from, state) do
    {:reply, state.server, state}
  end

  def handle_call({:login, user_name}, _from, state) do
    reply = GenServer.call(state.server, {:login, user_name, node()})
    {:reply, reply, state}
  end

  def handle_call({:logout}, _from, state) do
    reply = GenServer.call(state.server, :logout)
    {:reply, reply, state}
  end

  def handle_call({:say, text}, _from, state) do
    reply = GenServer.call(state.server, {:say, text})
    {:reply, reply, state}
  end

  def handle_call({:get_profile}, _from, state) do
    reply = state.profile
    {:reply, reply, state}
  end

  def handle_call({:set_profile, key, value}, _from, state) do
    new_profile = Map.put(state.profile, key, value)
    reply = {:ok, "set profile"}
    {:reply, reply, %{server: state.server, profile: new_profile}}
  end


  def handle_cast({:message, {from_user, from_server}, text}, state) do
    IO.puts("#{from_user}@#{from_server} says: #{text}")
    {:noreply, state}
  end

  def handle_cast(_msg, state) do
    {:noreply, state}
  end

  def get_chat_node() do
    GenServer.call(Person, :get_chat_node)
  end

  def login(user_name) do
    if (is_atom(user_name)) do
      GenServer.call(__MODULE__, {:login, to_string(user_name)})
    else
      GenServer.call(__MODULE__, {:login, user_name})
    end
  end


  def logout() do
    GenServer.call(__MODULE__, :logout)
  end

  def say(text) do
    GenServer.call(__MODULE__, {:say, text})
  end

  def users() do
    GenServer.call(get_chat_node(), :users)
  end

  def who(user_name, user_node) do
    GenServer.call(get_chat_node(), {:profile, user_name, user_node})
  end

  def set_profile(key, value) do
    GenServer.call(__MODULE__, {:set_profile, key, value})
  end

end
