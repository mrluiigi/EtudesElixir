defmodule Phone do
  require Record

  Record.defrecord(:phone, [number: "", start_date: "1900-01-01", start_time: "00:00:00", end_date: "1900-01-01", end_time: "00:00:00"])

end

defmodule PhoneETS do

  require Phone;

  def setup(file) do

    case :ets.info(:call_table) do
      :undefined -> false
      _ -> :ets.delete(:call_table)
    end

    :ets.new(:call_table, [:named_table, :bag, {:keypos, Phone.phone(:number) + 1}])


    {result, device} = File.open(file, [:read, :utf8])
    if result == :ok do
      add_rows(device)
    end

  end

  def add_rows(device) do
    data = IO.read(device, :line)
    cond do
      data != :eof  ->
                      [number, sdate, stime, edate, etime] = data |> String.trim() |> String.split(",")
                      :ets.insert(:call_table, Phone.phone( number: number,
                                                            start_date: gregorianize(sdate, "-"), start_time: gregorianize(stime, ":"),
                                                            end_date: gregorianize(edate, "-"), end_time: gregorianize(etime, ":")))
                      add_rows(device)

      data == :eof -> :ok
    end
  end


  @spec gregorianize(String.t(), String.t()) :: {number(), number(), number()}
  def gregorianize(string, delimiter) do
    [n1, n2, n3] = String.split(string, delimiter)
    {String.to_integer(n1), String.to_integer(n2), String.to_integer(n3)}
  end

  @spec summary() :: list
  def summary() do
    result = :ets.first(:call_table)
    summary(result, [])
  end

  @spec summary(String.t(), list) :: list
  def summary(key, list) do
    if key != :"$end_of_table" do
      actual = summary(key)
      summary(:ets.next(:call_table, key), list ++ [actual])
    else
      list
    end
  end

  @spec summary(number()) :: {String.t(), number()}
  def summary(number) do
    result = :ets.lookup(:call_table, number)
    total = List.foldl(result, 0, fn x, acc -> calculate_time(x) + acc end)
    {number, total}
  end

  @spec calculate_time(tuple) :: number()
  def calculate_time(registo) do
    c1 = :calendar.datetime_to_gregorian_seconds({Phone.phone(registo, :start_date), Phone.phone(registo, :start_time)})
    c2 = :calendar.datetime_to_gregorian_seconds({Phone.phone(registo, :end_date), Phone.phone(registo, :end_time)})
    div((c2-c1) + 59, 60)
  end


end
