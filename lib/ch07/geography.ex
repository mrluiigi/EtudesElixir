defmodule Country do
  defstruct name: "", language: "", cities: []
end

defmodule City do
  defstruct name: "", population: 0, latitude: 0.0, longitude: 0.0
end


defmodule Geography do

  @spec make_geo_list(String.t()) :: Country.t()
  def make_geo_list(file) do
    {_result, device} = File.open(file, [:read, :utf8])
    process_line(device, [])
  end

  defp process_line(device, geo_list) do
    data = IO.read(device, :line)
    case data do
      :eof  ->  File.close(device)
                geo_list

        _   ->  linha = String.split(String.trim(data), ",")
                new_list = process_info(linha, geo_list)
                process_line(device, new_list)
    end

  end


  defp process_info([country, language], geo_list) do
    [%Country{name: country, language: language, cities: []} | geo_list]
  end


  defp process_info([city, population, latitude, longitude], [hd|tail]) do
    new_cities = [%City{name: city, population: String.to_integer(population),
                        latitude: String.to_float(latitude), longitude: String.to_float(longitude)} | hd.cities]
    [%Country{ hd | cities: new_cities} | tail]
  end


  @spec total_population([Country], String.t()) :: number()
  def total_population(geo_list, language) do
    total_population(geo_list, language, 0)
  end

  @spec total_population([Country], String.t(), number()) :: number()
  def total_population([], _language, total) do
    total
  end

  def total_population([head|tail], language, total) do
    if head.language == language do
      new_total = calculate_total(head.cities, total)
      total_population(tail, language, new_total)
    else
      total_population(tail, language, total)
    end
  end


  @spec calculate_total([City.t()], number()) :: number()
  def calculate_total([], total) do
    total
  end

  def calculate_total([head|tail], total) do
    calculate_total(tail, total + head.population)
  end

end

