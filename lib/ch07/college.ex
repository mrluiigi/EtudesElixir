defmodule College do

  @spec make_room_list(String.t()) :: Map.t()
  def make_room_list(file) do
    {_result, device} = File.open(file, [:read, :utf8])
    room_list = Map.new()
    process_line(device, room_list)
  end

  defp process_line(device, room_list) do
    data = IO.read(device, :line)
    case data do
      :eof -> File.close(device)
              room_list

        _  -> new_list = process_room(data, room_list)
              process_line(device, new_list)
    end
  end

  defp process_room(data, room_list) do
    [_id, course, room] = String.split(String.trim(data), ",")
    course_list = Map.get(room_list, room)

    case course_list do
      nil ->  Map.put_new(room_list, room, [course])              # devolve o map
        _ ->  Map.put(room_list, room, [course | course_list])
    end

  end

end
