defmodule AskArea do

  @spec area() :: number()
  def area() do
    input = IO.gets("R)ectangle, T)riangle, or E)llipse: ")
    shape = char_to_shape(input)
    {d1, d2} = case shape do
      :rectangle  ->  get_dimensions("width", "base")
      :triangle   ->  get_dimensions("base", "height")
      :ellipse    ->  get_dimensions("major radius", "minor radius")
      :unknown    -> {String.first(input), 0}
    end
    calculate(shape, d1, d2)
  end

  @spec calculate(atom(), number(), number()) :: number()
  def calculate(shape, d1, d2) do
    cond do
      shape == :unknown -> IO.puts("Unknown shape")
      d1 < 0 or d2 < 0 -> IO.puts("Both numbers must be greater than or equal to zero")
      true -> Geom3.area(shape, d1, d2)
    end
  end

  @spec get_dimensions(String.t(), String.t()) :: {number(), number()}
  def get_dimensions(p1, p2) do
    n1 = get_number(p1)
    n2 = get_number(p2)
    {n1, n2}
  end


  @spec get_number(String.t()) :: number()
  def get_number(prompt) do
    a = String.trim(IO.gets("Enter #{prompt} > "))
    cond do
      Regex.match?(~r/^[+-]?\d+$/, a)  ->  String.to_integer(a)                       # int
      Regex.match?(~r/^[+-]?\d+\.\d+([eE][+-]?\d+)?$/, a)  -> String.to_float(a)    # float
      true -> :error
    end
  end


  @spec char_to_shape(String.t()) :: atom()
  def char_to_shape(initial) do
    initial
    |> String.upcase
    |> String.first
    |> case do
      "R" ->  :rectangle
      "T" -> :triangle
      "E" -> :ellipse
      _   -> :unknown
    end

  end


end
