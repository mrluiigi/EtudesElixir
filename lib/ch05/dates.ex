defmodule Dates do

  @spec date_parts(String.t()) :: [number()]
  def date_parts(data) do
      [y, m, d] = String.split(data, "-")
      [String.to_integer(y), String.to_integer(m), String.to_integer(d)]
  end

end
